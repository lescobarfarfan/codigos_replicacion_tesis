# codigos_replicacion_tesis

Códigos para replicar los resultados de la tesis "Analisis de conglomerados y de redes para la identificación de modelos de negocios en el sistema bancario Mexicano"

Los datos para los métodos de clustering son una versión pública de los datos originales usados en la tesis, se pueden descargar de la página:
https://datos.gob.mx/busca/dataset/serie-r01-catalogo-minimo/resource/ed164a91-bce4-4563-b285-bff7f6bd219a

Por su parte, las redes incluidas son simuladas y pueden diferir significativamente de las originales utilizadas para la tesis.